const { resolve } = require('path')

const DEFAULTS = {
  template: 'mdl',
}

const _templates = ['mdl', 'vm']

module.exports = function FormsModule(moduleOptions) {
  const options = Object.assign({}, DEFAULTS, moduleOptions, this.options.forms)

  options.componentsPath = resolve(__dirname, './_components')
  options.modulePath = resolve(__dirname)

  if (!_templates.includes(options.template)) {
    throw new Error('[FormsModule]: please choose templates of ' + (_templates.join(',')))
    return
  }

  this.addTemplate({
    src: resolve(__dirname, './_components/forms.components.template.js'),
    fileName: 'forms.components.template.js',
    options
  })

  this.addPlugin({
    src: resolve(__dirname, './_store/forms.store.template.js'),
    fileName: 'forms.store.plugin.js',
    options
  })



  this.addPlugin({
    src: resolve(__dirname, './_plugins/forms.plugin.js'),
    fileName: 'forms.plugin.js',
    options
  })
}
