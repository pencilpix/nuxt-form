import Vue from 'vue'
import FormsComponents from './forms.components.template.js'
import VeeValidate, { Validator } from 'vee-validate'

const options = <%= JSON.stringify(options) %>

Vue.use(VeeValidate, {
  events: 'blur|change',
})
Vue.use(FormsComponents)

export default async function ({ app, route }) {
  const localeParam = options.localeParam
  const lang = route.params[localeParam]

  if (!lang) {
    return
  }

  await import('vee-validate/dist/locale/' + lang)
    .then((msgs) => {
      Validator.localize(lang, msgs)
    })
    .catch(err => {
      console.log(err)
      return null
    })
}
