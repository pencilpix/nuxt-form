function createFormsStore() {
  return {
    namespaced: true,
    state: {},
    mutations: {},
    getters: {},
    actions: {},
  }
}


export default function ({ store }) {
  const opts = {}
  if (process.client) {
    opts.preserveState = true
  }

  store.registerModule('$form', createFormsStore(), opts)
}
