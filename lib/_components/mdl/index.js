import AppForm from './AppForm.vue';
import AppSingleInput from './AppSingleInput.vue';

export default {
  install(Vue) {
    Vue.component('AppForm', AppForm);
    Vue.component('AppSinleInput', AppSingleInput);
  },
};
