/* eslint-disable no-useless-escape */

/**
 * default rules for validation.
 * @type {Object}
 */
const DEFAULT_RULES = {
  required: true,
  max: 200,
  regex: /[\+|(00)]\d{2}[\-]?\d{2}[\-]?\d{8}/,
  date_format: 'dd MMM yyyy',
};

const DEFAULT_OPTIONS = {
  type: 'text',
  label: '',
  value: '',
  name: '',
  placeholder: '',
  rules: {},
  inputs: [],
  repeatable: false,
  dismiss: false,
  strict: true,
  multiple: false,
  param: '',
  paramValue: null,
  displayParams: [],
  action: '',
  getter: '',
  icon: '',
  chips: [],
  readonly: false,
  disabled: false,
};



/**
 * create a form input with rules of validations
 * @param {String}         type   input type
 * @param {String}         label  the text appear above the input
 * @param {String/Boolean} value  the default value of the input
 * @param {Object}         rules  rules to be extended.
 */
export default class FormInput {
  constructor(options) {
    this.options = Object.assign({}, Object.freeze(DEFAULT_OPTIONS), options);
    this.init();
    if (this.type === 'autocomplete' || this.type === 'location') {
      this.initAutocomp();
    }
  }

  init() {
    Object.keys(this.options).forEach((opt) => {
      switch (opt) {
        case 'rules':
          this[opt] = Object.assign({}, this.RULES, this.options.rules);
          break;

        case 'name':
          this[opt] = this.options.name || this.options.label
            .replace(/[\s|\W]/mg, '_').toLowerCase();
          break;

        case 'chips':
          this.chipsClasses = this.options[opt];
          break;

        default:
          this[opt] = this.options[opt];
          break;
      }
    });
  }

  initAutocomp() {
    this.autoCompleteHidden = true;

    if (this.multiple || this.type === 'location') {
      this[this.name] = [];
    }
  }

  getCopy(options = {}) {
    return Object.assign({}, this, options);
  }


  get RULES() {
    const rules = { required: DEFAULT_RULES.required };

    switch (this.type) {
      case 'tel':
        rules.regex = DEFAULT_RULES.regex;
        break;

      case 'date':
        rules.date_format = DEFAULT_RULES.date_format;
        rules.to = '1900';
        break;

      case 'email':
        rules.email = DEFAULT_RULES.email;
        break;

      default:
        rules.required = true;
        break;
    }

    return rules;
  }
}