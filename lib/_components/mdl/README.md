# Form Builder

form builder enables you to create a form with validation so fast and easy. it uses `custom events` and `vuex` actions and getters in some cases to load data. also it uses `vee-validate` to validate the data of the form.

## Getting started

include Form Builder to your component and create inputOptions as:

```js
import FormBuilder from './formbuilder';

export default {
  name: 'somecomponent',

  components: {
    FormBuilder,
    ...
  },

  data() {
    return {
      inputsOptions: [
        // each object represent an input.
        {
          type: 'email',
          label: 'Email Address',
          name: 'email',
          value: 'initial@some.value',
          placeholder: 'type your email address',
          rules: { required: true },
        },

        {
          type: 'autocomplete',
          label: 'Address',
          name: 'location',
          value: '',
          placeholder: '',
          param: 'geonameid',
          displayParams: ['country_name', 'admin_name'],
          getter: 'getLocation',
          action: 'loadLocations',
        }
      ],
    };
  },
};
```

then add template as:

```html
<form-builder
    :options="inputsOptions"
    @save="saveDate($event)">
  <!-- this button will added at the end of form -->
  <!-- and you can attach some event or detect -->
  <!-- some custom behavior in you component -->
  <md-button class="md-raised md-primary" type="submit">
    <md-icon>check</md-icon>
    Submit
  </md-button>
</form-builder>
```

## events:
it send one event `save` and the function attached to that event will recieve data like: 
```js
{
  'input_name': 'value in that input',
}
```

### options:

#### general options:
the following options required for all types of inputs.

option          | type            | descriptions
----------------|-----------------|---------------------------------------------
type            | {String}        | type of input and it's required `text`, `email`, `date`, `tel`, `autocomplete`, `textarea`, `checkbox` and `radio`.
name            | {String}        | the name of the input and it will be send in data object of the form as a key for the value of that input.
placeholder     | {String}        | optional text of input placeholder.
value           | {any}           | initial value of input.
label           | {String}        | the text that appear in the input label
as              | {String}        | the name of input that appear in the validation message.
rules           | {Object}        | object of validation rules that will override the default rules. for more rules of validation see [vee-validate](http://vee-validate.logaretm.com/index.html#available-rules).
repeatable      | {Boolean}       | shows add button that emit `repeatInput` event with value `options` represent the input options to be copied outside the component. default value `false`. repeat option does not work with `radio` and `checkbox` type.
dismiss         | {Boolean}       | shows remove button that emit `removeInput` event with value `options` object represent the input options to be removed from builder options outside the component. default value `false`. it does not work for `radio/checkbox` types.

#### checkbox options
checkbox has dedicated options
option          | type            | value
----------------|-----------------|-------------------------------------------------
inputs          | {Array[Object]} | {text, value, name} text appears next to checkbox, value true/false, name will be sent as key for the data.


#### radio options
radio has dedicated options
option          | type            | value
----------------|-----------------|-------------------------------------------------
inputs          | {Array[Object]} | [text, value} text appears next to checkbox, value that will be sent.



#### autocomplete
autocomplete has extra options

option          | type            | value
----------------|-----------------|-------------------------------------------------
action          | {String}        |  vuex action will receive the current value of input so that dispatch loading data from server. it should be string.
getter          | {String}        | vuex getter name which will get the result of some query from the store. it expected to be a function that receives the current value of input as argument and get `{query, content: []}` object.
displayParams   | {Array[String]} | `['key', 'key2']` the keys that will merged together to get the final input value that appears to the user.
param           | {String}        | the key that will be sent to the server `{name: param}` as `{location: 123456}`
strict          | {Boolean}       | force user to choose only from the list or not default value `true`
multiple        | {Boolean}       | user can select or type multiple values appears as chips. default value `false`
inputs          | {Array}         | in case of `multiple = true` this option is required that hold the initial values.



### examples

#### regular inputs
`email`, `number`, `text`, `date`, `textarea`, `tel`, `password`

```js
// ....

data() {
  return {
    builderOptions: [
      {
        type: 'email', // can be any of regular inputs label above.
        label: 'Email Address',
        name: 'email',
        value: 'initial@some.value',
        placeholder: 'type your email address',
        rules: { required: true , email: true }, // rules as it is in vee-validate.
        repeatable: true, // if you need to capture this input to repeat later.
                          //on click to add button
        dismiss: true, // if you need to capture this input options
                       //to remove on click to remove button.
      },
      {
        type: 'textarea', 
        label: 'Description',
        name: 'description',
        value: 'initial@some.value',
        placeholder: 'type description in max 144 letters',
        rules: { required: true , max: 144 }, // rules as it is in vee-validate.
      },
    ],
  };
},

// ...
```


#### checkbox inputs
```js
// ....

data() {
  return {
    builderOptions: [
      {
        type: 'checkbox',
        label: 'Target',
        name: 'target',
        value: '',
        rules: { required: true }, // rules as it is in vee-validate.
        inputs: [
          { text: 'UI/UX designers', value: false, name: 'designers' },
          { text: 'back end developers', value: false, name: 'developers' },
        ],
      },
    ],
  };
},

// ...
```



#### radio inputs
```js
// ....

data() {
  return {
    builderOptions: [
      {
        type: 'radio',
        label: 'Target',
        name: 'target',
        value: '',
        rules: { required: true }, // rules as it is in vee-validate.
        inputs: [
          { text: 'UI/UX designers', value: 'designers' },
          { text: 'back end developers', value: 'developers' },
        ],
      },
    ],
  };
},

// ...
```


#### autocomplete inputs
```js
// ....

data() {
  return {
    builderOptions: [
      {
        type: 'autocomplete',
        label: 'Location',
        name: 'location',
        value: '',
        rules: { required: true }, // rules as it is in vee-validate.
        getter: 'getLocation', // getter name it should be a function not a regular getter.
        action: 'loadLocation', // vuex action that will recieve the query and start fetching result from service.
        param: 'geonameid', // it will search in result object for that key and return it's value.
        displayParams: ['country_name', 'admin_name'], // that will be rendered in the input value as `value of counter - value of admin`
        multiple: true,
        strict: false, // turning off strict mode to auto complete list
        inputs: [
          { text: 'UI/UX designers', param: 'designers' },
          { text: 'back end developers', param: 'developers' },
        ],
      },

      {
        type: 'autocomplete',
        label: 'Location',
        name: 'location',
        value: '',
        rules: { required: true }, // rules as it is in vee-validate.
        getter: 'getLocation', // getter name it should be a function not a regular getter.
        action: 'loadLocation', // vuex action that will recieve the query and start fetching result from service.
      },
    ],
  };
},

// ...
```