import MultiInputForm from '<%= options.componentsPath %>/<%= options.template %>/MultiInputForm.vue'

export default {
  install(Vue) {
    Vue.component('MultiInputForm', MultiInputForm)
  }
}