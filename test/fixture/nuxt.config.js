const { resolve } = require('path')

module.exports = {
  rootDir: resolve(__dirname, '../..'),
  srcDir: __dirname,
  dev: false,
  render: {
    resourceHints: false
  },


  head: {
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons'},
      { rel: 'stylesheet', href: 'https://code.getmdl.io/1.3.0/material.min.css'},
    ],

    script: [
      { src: 'https://code.getmdl.io/1.3.0/material.min.js'},
    ]
  },

  modules: [
    ['@@', { localeParam: 'lang' }]
  ]
}
