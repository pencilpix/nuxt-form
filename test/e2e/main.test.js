const { Nuxt, Builder } = require('nuxt')
const request = require('request-promise-native')

const config = require('../fixture/nuxt.config')

const PORT = 4000
const TIMEOUT = 50000
const url = path => `http://localhost:${PORT}${path}`
const get = path => request(url(path))

let nuxt

module.exports = {
  // stub
  before: async function (browser, done) {
    nuxt = new Nuxt(config)
    await new Builder(nuxt).build()
    await nuxt.listen(PORT)

    done()
  },


  'some thing to assert': function (client) {
    client
      .url('http://localhost:4000')
      .waitForElementVisible('body')
      .assert.title('home')
      .end()
  },


  // teardown
  after: async function() {
    await nuxt.close()
  },
}

