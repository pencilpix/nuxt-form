module.exports = {
  collectCoverage: true,
  testPathIgnorePatterns: [
    '/node_modules/',
    '/e2e/',
    '/jest\.\w+\d?\w+\.config.js/',
  ],
  moduleFileExtensions: [
    'js',
    'json',
    'vue'
  ],
  transform: {
    '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
    '.*\\.(vue)$': '<rootDir>/node_modules/vue-jest'
  },
  snapshotSerializers: [
    '<rootDir>/node_modules/jest-serializer-vue'
  ],
  coverageDirectory: '<rootDir>/coverage',
  collectCoverageFrom: [
    '!**/test/**',
    '!**/node_modules/**',
    '**/{components,store}/**/*.{js,vue}',
    '**/modules/**/_{components,store}/*.{js,vue}',
  ],

  coveragePathIgnorePatterns: [
    '<rootDir>/node_modules/',
    '<rootDir>/\.\w+\.?\w?',
  ]
}
